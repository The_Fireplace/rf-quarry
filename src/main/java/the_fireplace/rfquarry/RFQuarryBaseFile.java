package the_fireplace.rfquarry;

import the_fireplace.rfquarry.mmplfiles.BlockFrame;
import cpw.mods.fml.common.Mod;
import cpw.mods.fml.common.Mod.EventHandler;
import cpw.mods.fml.common.Mod.Instance;
import cpw.mods.fml.common.event.FMLPreInitializationEvent;
/*
 * 
 * @author The_Fireplace
 * 
 */
@Mod(name="RF Quarry", modid="rfquarry", version="pre1.0.0", acceptedMinecraftVersions="1.7.10")
public class RFQuarryBaseFile {
@Instance("rfquarry")
public static RFQuarryBaseFile instance;

public static BlockFrame frameBlock;
public static int legacyPipeModel;

@EventHandler
public void PreInit(FMLPreInitializationEvent event){
	
}
}
