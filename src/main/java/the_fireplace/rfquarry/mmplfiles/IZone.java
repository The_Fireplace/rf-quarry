package the_fireplace.rfquarry.mmplfiles;

import java.util.Random;

/**
 * This file is distributed under the terms of the Minecraft Mod Public
 * License 1.0, or MMPL. Please check the contents of the license located in
 * http://www.mod-buildcraft.com/MMPL-1.0.txt
 */
public interface IZone {

	double distanceTo(BlockIndex index);

	boolean contains(double x, double y, double z);

	BlockIndex getRandomBlockIndex(Random rand);
}
