package the_fireplace.rfquarry.mmplfiles;

/**
 * This file is distributed under the terms of the Minecraft Mod Public
 * License 1.0, or MMPL. Please check the contents of the license located in
 * http://www.mod-buildcraft.com/MMPL-1.0.txt
 */
public interface IAreaProvider {
	int xMin();

	int yMin();

	int zMin();

	int xMax();

	int yMax();

	int zMax();

	/**
	 * Remove from the world all objects used to define the area.
	 */
	void removeFromWorld();
}
